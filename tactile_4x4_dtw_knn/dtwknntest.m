clear all; close all; clc;
cd NU_objects
mycsv = dir('*.csv');
alldata = {};
alllabels = {};
ind = 0;
for i=1:numel(mycsv)
    name = mycsv(i).name;
    label = name(1:end-4);
    temp = regexp(label, '[0-9]', 'once');
    if (~isempty(temp))
        label = label(1:temp - 1);
    end
    data = importdata(name);
    colheaders = data.colheaders;
    indxs = contains(colheaders, "img");
    imgdata = data.data(:, indxs);
    
    current_state_urindx = contains(colheaders, "current_state_ur");
    curstate = data.data(:, current_state_urindx);
    
    len = size(curstate, 1);
    %plot(curstate);
    %disp(label);
    %pause();
    
    foundstart = 0;
    foundend = 0;
    mystart = 0;
    myend = 0;
    cycle = 0;
    for j=1:len
        val = curstate(j);
        if (val == 1 || val == 2)
            if (foundstart == 0)
                foundstart = 1;
                mystart = j;
            end
        end
        if (val == 3)
            if (foundend == 0)
                foundend = 1;
                myend = j - 1;
                cycle = 1;
                mydata = imgdata(mystart:myend, :);
                ind = ind + 1;
                alldata{ind} = mydata;
                alllabels{ind} = label;
            end
        end
        if (val == 0 && cycle == 1)  
            foundstart = 0;
            foundend = 0;
            mystart = 0;
            myend = 0;
            cycle = 0;
        end
        if (val == 0 && foundend == 0 && foundstart ~= 0)
            % missing 3
            myend = j - 1;
            cycle = 1;
            mydata = imgdata(mystart:myend, :);
            ind = ind + 1;
            alldata{ind} = mydata;
            alllabels{ind} = label;
            
            foundstart = 0;
            foundend = 0;
            mystart = 0;
            myend = 0;
            cycle = 0;
        end
    end
end
%%
cd ..
features1 = {};
features2 = {};
featlabels = {};
for i=1:numel(alldata)
    label = alllabels{i};
    data = alldata{i};
    featlabels{i} = label;
    
    f1s = [];
    f2s = [];
    for j=1:size(data, 1)
        piece = data(j, :);
        f1 = mean(piece);
        f2 = std(piece);
        f1s(j) = f1;
        f2s(j) = f2;
    end
    features1{i} = f1s;
    features2{i} = f2s;
end
%%
features3 = {};
for i=1:numel(features1)
    d1 = features1{i};
    d2 = features2{i};
    features3{i} = horzcat(d1, d2);    
end

labelsstr = unique(featlabels);
myperm = randperm(numel(featlabels));

shuflabels = featlabels(myperm);
shufdata1 = features1(myperm);
shufdata2 = features2(myperm);
shufdata3 = features3(myperm);
shuflabelsnum = zeros(1, numel(featlabels));
for i=1:numel(shuflabels)
    [~, mylabel] = max(contains(labelsstr, shuflabels{i}));   
    shuflabelsnum(i) = mylabel;
end

trdatasize = round(0.8*numel(shufdata1));
traindata1 = shufdata1(1:trdatasize);
traindata2 = shufdata2(1:trdatasize);
traindata3 = shufdata2(1:trdatasize);
trainlabels = shuflabelsnum(1:trdatasize);

testdata1 = shufdata1(trdatasize + 1:end);
testdata2 = shufdata2(trdatasize + 1:end);
testdata3 = shufdata2(trdatasize + 1:end);
testlabels = shuflabelsnum(trdatasize + 1:end);

% store predictions of testing data in the following variable
Mtst = numel(testlabels);
preds1 = zeros(1, Mtst);
preds2 = zeros(1, Mtst);
preds3 = zeros(1, Mtst);
K = numel(labelsstr);
for i=1:Mtst  
    pred1 = dtwknn(traindata1, trainlabels, testdata1{i}, K);
    preds1(i) = pred1;
    
    pred2 = dtwknn(traindata2, trainlabels, testdata2{i}, K);
    preds2(i) = pred2;
    
    pred3 = dtwknn(traindata3, trainlabels, testdata3{i}, K);
    preds3(i) = pred3;
end
% calculate the accuracy
acc1 = sum(preds1 == testlabels) / numel(preds1);
acc2 = sum(preds2 == testlabels) / numel(preds2);
acc3 = sum(preds3 == testlabels) / numel(preds3);
disp(acc1);
disp(acc2);
disp(acc3);

%%
Mtst = numel(traindata1);
preds1 = zeros(1, Mtst);
preds2 = zeros(1, Mtst);
preds3 = zeros(1, Mtst);
K = numel(labelsstr);
for i=1:Mtst  
    pred1 = dtwknn(traindata1, trainlabels, traindata1{i}, K);
    preds1(i) = pred1;
    
    pred2 = dtwknn(traindata2, trainlabels, traindata2{i}, K);
    preds2(i) = pred2;
    
    pred3 = dtwknn(traindata3, trainlabels, traindata3{i}, K);
    preds3(i) = pred3;
end
% calculate the accuracy
acc1 = sum(preds1 == trainlabels) / numel(preds1);
acc2 = sum(preds2 == trainlabels) / numel(preds2);
acc3 = sum(preds3 == trainlabels) / numel(preds3);
disp(acc1);
disp(acc2);
disp(acc3);