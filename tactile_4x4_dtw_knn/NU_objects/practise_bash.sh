#!/bin/bash
# declare STRING variable
#echo `rostopic list`
#echo `rostopic echo -b Ball150.bag -p /plot_matlab >> Ball150.csv`

filenames=`ls ./*.bag`
for entry in $filenames
do
  csvname=$(echo $entry | cut -c 3-${#entry})
  inFile=$csvname
  csvname="$(echo $csvname | cut -f 1 -d '.').csv"
echo `rm $csvname`   
echo `rostopic echo -b $inFile -p /plot_matlab >> $csvname`
done




