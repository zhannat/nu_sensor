// knndtw.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"  
#include <limits>
#include <math.h>
#include <algorithm> 
#include <vector> 
#include <fstream>
#include <iostream>
#include <iterator>
#include <string>
#include <sstream> 
#include <time.h>

#define ROWS 1490
#define COLS 55
std::vector<std::vector<double>> features, featuresTest;
double reducedDimension[ROWS][COLS];

class Pair {
private:
	double value;
	double label;

public:
	Pair(double v, double l) {
		value = v;
		label = l;
	}

	double getValue() const {
		return value;
	}

	double getLabel() const {
		return label;
	}

	void setValue(double v) {
		value = v;
	}

	void setLabel(double l) {
		label = l;
	} 
};

bool operator<(Pair const & L, Pair const & R) { // The operator takes const references - it can compare const objects
	return L.getValue() < R.getValue();
}

double **DTW = NULL;
void initDtw() {
	int size = 1500;
	int n = size;
	int m = size;
	DTW = new double*[n + 1];
	for (int i = 0; i <= n; i++) {
		DTW[i] = new double[m + 1];
	}
}

double DTWDistance(double *s, double *t) {
	/*double **DTW = new double*[n + 1];
	for (int i = 0; i <= n; i++) {
		DTW[i] = new double[m + 1];
	}*/
	  
	int n = COLS;
	int m = COLS;
	double dblmax = std::numeric_limits<double>::max();
	for (int i = 1; i <= n; ++i) {
		DTW[i][0] = dblmax;
	}
	for (int i = 1; i <= m; ++i) {
		DTW[0][i] = dblmax;
	}
	DTW[0][0] = 0; 

	double cost = 0;
	double sval = 0; 

	for (int i = 1; i <= n; i+=1) {
		sval = s[i - 1]; 
		for (int j = 1; j <= m; j+=1) {
			DTW[i][j] = fabs(sval - t[j - 1]) + fmin(fmin(DTW[i - 1][j], DTW[i][j - 1]), DTW[i - 1][j - 1]);
		}
	} 

	if (false) {
		for (int i = 1; i <= n; ++i) {
			for (int j = 1; j <= m; ++j) {
				printf("%.1f\t", DTW[i][j]);
			}
			printf("\n");
		}
	}

	double result = DTW[n][m];
	/*for (int i = 0; i <= n; i++) {
		delete []DTW[i];
	}
	delete[]DTW;*/
	delete[]t;
	return result;
}

std::vector<std::string> split(const char *phrase, std::string delimiter) {
	std::vector<std::string> list;
	std::string s = std::string(phrase);
	size_t pos = 0;
	std::string token;
	while ((pos = s.find(delimiter)) != std::string::npos) {
		token = s.substr(0, pos);
		list.push_back(token);
		s.erase(0, pos + delimiter.length());
	}
	return list;
}

void loadData() {
	std::ifstream file("Z:\\tactile\\Zhanat(muzhik)\\zhanat\\nu_sensor\\training.csv");

	std::string line = "";
	// Iterate through each line and split the content using delimeter
	while (getline(file, line))
	{ 
		line.append(",");
		std::vector<double> vec;
		std::vector<std::string> splitted = split(line.c_str(), ","); 
		for (int i = 0; i < splitted.size(); ++i) {
			double val = atof(splitted.at(i).c_str());
			vec.push_back(val);
		}
		features.push_back(vec);
	}
	// Close the File
	file.close();

	std::ifstream file2("Z:\\tactile\\Zhanat(muzhik)\\zhanat\\nu_sensor\\testing.csv");

	line = "";
	// Iterate through each line and split the content using delimeter
	while (getline(file2, line))
	{
		line.append(",");
		std::vector<double> vec;
		std::vector<std::string> splitted = split(line.c_str(), ",");
		for (int i = 0; i < splitted.size(); ++i) {
			double val = atof(splitted.at(i).c_str());
			vec.push_back(val);
		}
		featuresTest.push_back(vec);
	}
	// Close the File
	file2.close();

	std::ifstream file3("Z:\\tactile\\Zhanat(muzhik)\\zhanat\\nu_sensor\\reducedDimension.csv");

	line = "";
	// Iterate through each line and split the content using delimeter
	int rows = 0;
	int cols = 0;
	while (getline(file3, line))
	{
		line.append(",");
		std::vector<std::string> splitted = split(line.c_str(), ",");
		cols = 0;
		for (int i = 0; i < splitted.size(); ++i) {
			double val = atof(splitted.at(i).c_str());
			reducedDimension[rows][cols] = val;
			cols++;
		}
		rows++;
	}
	// Close the File
	file3.close();  
}

double *reduceDimension(double *data) {
	double *result = new double[COLS];
	double val = 0;
	for (int i = 0; i < COLS; ++i) {
		val = 0;
		for (int j = 0; j < ROWS; ++j) {
			val += data[j] * reducedDimension[j][i];
		}
		result[i] = val;
	}
	return result;
}

int classifyKnnDtw(std::vector<double> x) {
	int K = 2;
	std::vector<Pair> pairs;
	
	for (int i = 0; i < features.size(); ++i) {
		std::vector<double> feature = features.at(i);
		std::vector<double>::const_iterator first = feature.begin() + 1;
		std::vector<double>::const_iterator last = feature.end();
		std::vector<double> newVec(first, last);

		double label = feature.at(0);
		double distance = DTWDistance(newVec.data(), reduceDimension(x.data()));
		Pair pair(distance, label);
		pairs.push_back(pair);
		
	} 

	std::sort(pairs.begin(), pairs.end()); 

	int class1 = 0;
	int class2 = 0;

	for (int i = 0; i < K; ++i) {
		int label = (int)pairs.at(i).getLabel();
		if (label == 1) {
			class1++;
		}
		else {
			class2++;
		}
	}
	if (class1 > class2) {
		return 1;
	}
	else {
		return 2;
	}
}
 
int main()
{
	initDtw(); 

	loadData();

	int correct = 0;
	int total = 0;
	
	for (int i = 0; i < featuresTest.size(); ++i) {
		std::vector<double> myfeat;
		std::vector<double> copyfrom = featuresTest.at(i);
		int label = (int)copyfrom.at(0);

		for (int i = 1; i < copyfrom.size(); ++i) {
			myfeat.push_back(copyfrom.at(i));
		} 

		clock_t t = clock();
		int result = classifyKnnDtw(myfeat); 
		t = clock() - t;
		printf("It took me %d clicks (%f seconds).\n", t, ((float)t) / CLOCKS_PER_SEC);

		if (result == label) {
			++correct;
		}
		++total;
		printf("corrects: %d, total: %d, accuracy: %f\n", correct, total, 100 * (float)correct / (float)total);
	}

	printf("corrects: %d, total: %d, accuracy: %f\n", correct, total, 100 * (float)correct / (float)total);

	return 0;
}

