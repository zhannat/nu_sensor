name = 'C:\Development\projects\zhanat\nu_sensor\csv_experiments\exploration_palpation\nice_exploration_bug_found_full_sure_double2.csv';
data = importdata(name);
colheaders = data.colheaders;
%%
indxs = [42:44, 81:numel(colheaders)];
%%
imgdata = data.data(:, indxs);

extdata = [];
for i=1:3:size(imgdata,2)
    data1 = imgdata(:, i:i+2);
    extdata = vertcat(extdata, data1);
end

hist(extdata(:,1));

plot3(extdata(:, 1), extdata(:, 2), extdata(:, 3), 'o');


%%
for i=1:size(imgdata, 1)
    vec = imgdata(i,:);
    mymax = max(vec);
    if (mymax > 50)
        disp(i);
        break;
    end
end
%%
indxs = contains(colheaders, "field.joint_angles0");
imgdata = data.data(:, [69:numel(colheaders)]);

for i=1060:20:size(imgdata, 1)
    disp(i);
    vec = imgdata(i,:);
    matr = reshape(vec, 4, 4);
    surf(matr);
    drawnow;
    pause(1);
end

%%
extdata = imgdata;
plot3(extdata(:, 1), extdata(:, 2), extdata(:, 3), 'o');
