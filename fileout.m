fileID = fopen('trainingdata.txt','w');
for i=1:trdatasize
    val = traindata{i};
    label = uint8(trainlabels(i));
    fprintf(fileID,'%d, ',label);
    for j=1:numel(val)
        fprintf(fileID,'%f, ',val(j));
    end
    fprintf(fileID,'\n');
end

fclose(fileID);