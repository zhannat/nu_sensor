m = [];
mcell = {};
for i=1:size(features1,2)
    myfeat = features1{i};
    myfeat = myfeat(1:1490);
    m = [m; myfeat];
    mcell{i} = myfeat;
end

myperm = randperm(numel(featlabels));

shuflabels = featlabels(myperm);
shufdata1 = mcell(myperm); 
shuflabelsnum = shuflabels;

trdatasize = round(0.8*numel(shufdata1));
traindata1 = shufdata1(1:trdatasize); 
trainlabels = shuflabelsnum(1:trdatasize);

testdata1 = shufdata1(trdatasize + 1:end); 
testlabels = shuflabelsnum(trdatasize + 1:end);

traindataMat1 = [];
for i=1:size(traindata1,2)
    myfeat = traindata1{i}; 
    traindataMat1 = [traindataMat1; myfeat];
end

[coeff,score,latent] = pca(traindataMat1);
numberOfDimensions = 55; 
reducedDimension = coeff(:,1:numberOfDimensions);
reducedData = traindataMat1 * reducedDimension;

traindataMat1 = reducedData;
traindataCell1 = {};
for i=1:size(traindata1,2)
    myfeat = traindataMat1(i, :); 
    traindataCell1{i} = myfeat;
end

Mtst = size(testdata1, 2);
preds1 = zeros(1, Mtst); 
K = numel(labelsstr);
for i=1:Mtst  
    pred1 = dtwknn(traindataCell1, trainlabels, testdata1{i}*reducedDimension, K);
    preds1(i) = pred1; 
end
% calculate the accuracy
acc1 = sum(preds1 == testlabels) / numel(preds1); 
disp(acc1); 
disp(sum(preds1 == testlabels))
disp(numel(preds1));

%%
rData1 = reducedData(trainlabels == 1, :);
rData2 = reducedData(trainlabels == 2, :);
hold on;
scatter3(rData1(:,1),rData1(:,2),rData1(:,3),'MarkerEdgeColor','k',...
        'MarkerFaceColor',[1 0 0]);
scatter3(rData2(:,1),rData2(:,2),rData2(:,3),'MarkerEdgeColor','k',...
        'MarkerFaceColor',[0 0 1]);
hold off;

%% 

testingDataMat = [];
for i=1:size(testdata1,2)
    myfeat = testdata1{i}; 
    testingDataMat = [testingDataMat;myfeat];
end
trainDataWithLabels = [trainlabels' traindataMat1];
testDataWithLabels = [testlabels' testingDataMat];
csvwrite('training.csv', trainDataWithLabels);
csvwrite('testing.csv', testDataWithLabels); 
csvwrite('reducedDimension.csv', reducedDimension);
